==========
ELBEstream
==========

---------------------------
Error Latched BuffEr stream
---------------------------

:Manual section: 1

SYNOPSIS
========

::

   ./elbestream
	[-d <directory>]
	[-i <input buffer size> (default: 1048576 bytes)]
	[-o <output buffer size> (default: 1048576 bytes)]
	[-m <minimum output size>]
	--   try command ...
	---- fin command ...

*fin command* needs to contain one ``%f`` argument which is replaced
by a filename which *fin command* should read input data from.

EXAMPLE
-------

::

   ./elbestream -m 131 \
     --   /usr/bin/ffmpeg -i - -ac 2 -f wav pipe:1 \
     ---- /usr/bin/ffmpeg -i %f -ac 2 -f wav pipe:1

DESCRIPTION
===========

Programs which usually work in *streaming* mode, taking their input
from `stdin(3)` and writing to `stdout(3)` might fail for some inputs.

For example,

::

  /usr/bin/ffmpeg -i - -ac 2 -f wav pipe:1 <FILE.aac >t.wav

might throw errors like::

  [mov,mp4,m4a,3gp,3g2,mj2 @ 0x560855efb900] stream 0, offset 0x28: partial file
  pipe:: Invalid data found when processing input
  ...
  Output file is empty, nothing was encoded (check -ss / -t / -frames parameters if used)

while::

  /usr/bin/ffmpeg -i FILE.aac -ac 2 -f wav pipe:1 >t.wav

works just fine.

This program aims to solve this case like these in a generic,
efficient manner, trying to use the *streaming* case whenever possible
and only failing back to a buffered file mode if necessary.

First, the input buffer is filled from `stdin(3)`.

Next, *try command* is executed on the input buffer with its output
buffered. Once either all of the input buffer has been written to *try
command*, or the output buffer is full, it is determined if the
program is classified as failed or considered successful (so far,
because not all input has necessarily been processed).

* Obviously, if the *try command* process has exited with failure, it
  is considered to have failed.

* Also, if it has not output at least as many bytes as specified by
  the ``-m`` argument, it is considered to have failed and gets
  terminated by elbestream.

If *try command* has not failed by these checks, it is left running
on the remainder of `stdin(3)`, with its output being sent to
`stdout(3)` as if only *try command* was run to begin with.

If, however, *try command* is considered to have failed, the input
buffer and the rest of `stdin(3)` are written to a temporary file
created in the directory specified by the ``-d`` argument or, if it is
not given, the current working directory. Then, the ``%f`` argument of
*fin command* is replaced with the name of that file and *fin command*
is run.

The ``-d`` argument can not be the empty string.

IMPLEMENTATION DETAILS
======================

This tool uses the `splice(2)` and `vmsplice(2)` systemcalls on Linux
if they are available and usable on the respective file descriptors in
an attempt to reduce overhead of the I/O operations involved. It falls
back to ordinary `read(2)`\ /\ `write(2)` loops.

INSTALLATION
============

See `INSTALL.rst <INSTALL.rst>`_ in the source repository.

SUPPORT
=======

For community support, please use `Gitlab Issues`_.

For commercial support, please contact info@uplex.de

.. _Gitlab Issues: https://gitlab.com/uplex/misc/elbestream/-/issues

COPYRIGHT
=========

::

  Copyright 2021 UPLEX Nils Goroll Systemoptimierung
  All rights reserved

  Author: Nils Goroll <nils.goroll@uplex.de>

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.
