#!/bin/bash

set -eux

cat=$(whereis -b cat | awk '{ print $2 }')
false=$(whereis -b false | awk '{ print $2 }')

t=$(date)
for i in 1 4 10 29 30 31 40 ; do
    for o in 1 4 10 29 30 31 40 ; do
	r=$(echo "${t}" | ./elbestream -i $i -o $o -- $cat \
				  ---- $cat %f)
	if [[ "${r}" != "${t}" ]] ; then
	    echo >&2 FAIL
	    exit 9
	fi
	r=$(echo "${t}" | ./elbestream -i $i -o $o -m 1 -- $false \
				  ---- $cat %f)
	if [[ "${r}" != "${t}" ]] ; then
	    echo >&2 FAIL
	    exit 9
	fi
    done
done

# empty -d argument
if ./elbestream -d '' -i $i -o $o -m 1 -- $false  ---- $cat %f ; then
    echo >&2 FAIL
    exit 9
fi

# -d OK
if ! echo "${t}" | ./elbestream -d /tmp -i $i -o $o -m 1 -- $false \
			      ---- $cat %f ; then
    echo >&2 FAIL
    exit 9
fi

# -d does not exist
if echo "${t}" | ./elbestream -d xxx -i $i -o $o -m 1 -- $false \
			      ---- $cat %f ; then
    echo >&2 FAIL
    exit 9
fi
