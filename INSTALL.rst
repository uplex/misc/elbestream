INSTALLATION
============

Building from source
~~~~~~~~~~~~~~~~~~~~

This tool should build like any other autotools-supported tool::

    ./autogen.sh
    ./configure
    make
    make check
    sudo make install

The ``./autogen.sh`` step is only needed when building from the git
repository.
