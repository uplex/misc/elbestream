/*
 *
 * Copyright 2021 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define _GNU_SOURCE

#include "config.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>
#include <stdint.h>

#define FALLBACK_BUF (64 * 1024)
#define DEF_IBUF (1024 * 1024)
#define DEF_OBUF (1024 * 1024)

#ifdef DEBUG
#define DBG(...) fprintf(stderr, "dbg" __VA_ARGS)
#else
#define DBG(...) (void)0
#endif

// assert-ish
#include <assert.h>
/* Assert zero return value */
#define AZ(foo)         do { assert((foo) == 0); } while (0)
#define AN(foo)         do { assert((foo) != 0); } while (0)

#define closef(fd)				\
	do {					\
		assert(fd >= 0);		\
		AZ(close(fd));			\
		fd = -1;			\
	} while (0)

#define closefif(fd) if (fd >= 0) closef(fd)

// from varnish-cache
/* Safe printf into a fixed-size buffer */
#define bprintf(buf, fmt, ...)						\
	do {								\
		int ibprintf;						\
		ibprintf = snprintf(buf, sizeof buf, fmt, __VA_ARGS__); \
		assert(ibprintf >= 0 && ibprintf < (int)sizeof buf);	\
	} while (0)

static void
blockf(int fd)
{
	int i;

	i = fcntl(fd, F_GETFL);
	assert(i != -1);
	i &= ~O_NONBLOCK;
	AZ(fcntl(fd, F_SETFL, i));
}

static void
usage(char * const argv[])
{
	fprintf(stderr, "Usage: %s\n"
	    "\t[-d <directory>]\n"
	    "\t[-i <input buffer size> (default: %d bytes)]\n"
	    "\t[-o <output buffer size> (default: %d bytes)]\n"
	    "\t[-m <minimum output size>]\n"
	    "\t--   try command ...\n"
	    "\t---- fin command ...\n\n",
	    argv[0], DEF_IBUF, DEF_OBUF);
}

static void
argerr(char * const argv[], const char *err, const char *where)
{
	if (errno != 0)
		fprintf(stderr, "%s: error before %s: %s (%d)\n\n",
		    err, where, strerror(errno), errno);
	else
		fprintf(stderr, "%s: error before %s\n\n",
		    err, where);
	usage(argv);
	exit(EXIT_FAILURE);
}

static void
pexit(const char *e) {
	perror(e);
	exit(EXIT_FAILURE);
}

/*
 * fork/exec subprocess and return pid and stdin/stdout file descriptors
 */
static pid_t
subproc(char * const *argv, char * const envp[], int *writefd, int *readfd)
{
	int pin[2], pout[2];
	pid_t pid;

	AN(argv);
	AN(envp);
	AN(writefd);
	AN(readfd);

	AZ(pipe2(pin, O_NONBLOCK));
	AZ(pipe2(pout, 0));
	pid = fork();
	if (pid < 0)
		pexit("fork");
	if (pid == 0) {
		if (dup2(pin[STDIN_FILENO], STDIN_FILENO) != STDIN_FILENO)
			pexit("dup2(STDIN_FILENO)");
		if (dup2(pout[STDOUT_FILENO], STDOUT_FILENO) != STDOUT_FILENO)
			pexit("dup2(STDOUT_FILENO)");
		closef(pin[STDIN_FILENO]);
		closef(pin[STDOUT_FILENO]);
		closef(pout[STDIN_FILENO]);
		closef(pout[STDOUT_FILENO]);
		blockf(STDIN_FILENO);
		blockf(STDOUT_FILENO);
		if (execve(argv[0], argv, envp) == -1)
			pexit("execve(argv)");
	}
	closef(pin[STDIN_FILENO]);
	*writefd = pin[STDOUT_FILENO];
	closef(pout[STDOUT_FILENO]);
	*readfd = pout[STDIN_FILENO];

	return (pid);
}

#if HAVE_VMWRITE
/* vmsplice with fallback to write() */
static ssize_t
vmwrite(int fd, const char *buf, size_t count)
{
	ssize_t r, written = 0;
	struct iovec iov[1];
	unsigned int flags = SPLICE_F_MORE | SPLICE_F_MOVE;
	int i;

	i = fcntl(fd, F_GETFL);
	assert(i != -1);
	if (i & O_NONBLOCK)
		flags |= SPLICE_F_NONBLOCK;

	while (count > 0) {
		iov[0].iov_base = (void *)buf;
		iov[0].iov_len = count;
		r = vmsplice(fd, iov, 1UL, flags);
		if (r == -1) {
			assert (errno != EINVAL);
			if (errno == EBADF)
				return (written + write(fd, buf, count));
			return (written ? written : -1);
		}

		assert(r >= 0);
		assert((size_t)r <= count);
		buf += r;
		written += r;
		count -= (size_t)r;
	}
	return (written);
}
#else
#define vmwrite(fd, buf, count) write(fd, buf, count)
#endif

#define NFD (nfds_t)2

/* try to stream buffer and return bytes buffered */
static ssize_t
trystream(const char *ibuf, size_t *isz, char *obuf, size_t obufsz,
    int *out, const int *in, int complete)
{
	ssize_t r, osz = 0;
	nfds_t i;
	struct pollfd fds[NFD] = {
		{ .fd = *in,
		  .events = POLLIN,
		  .revents = 0},
		{ .fd = *out,
		  .events = POLLOUT,
		  .revents = 0}};

	while ((*isz > 0 || complete) && obufsz > 0) {
		errno = 0;
		r = poll(fds, NFD, -1);
		if (r < 0) {
			assert(errno == EINTR);
			continue;
		}
		AN(r);
		for (i = 0; i < NFD; i++) {
			if (fds[i].revents == 0)
				continue;
			AZ(fds[i].revents & POLLNVAL);
			if (fds[i].revents & POLLERR) {
				perror("poll err");
				return (-1);
			}
			if (fds[i].revents & POLLHUP &&
			    fds[i].events & POLLOUT &&
			    *isz > 0) {
				perror("hung up before ibuf complete");
				return (-1);
				// else if HUP on POLLIN, it might have
				// processed completely, we need to check
				// wait status
			}

			if (fds[i].revents & POLLOUT) {
				r = vmwrite(fds[i].fd, ibuf, *isz);
				if (r < 0) {
					perror("write");
					return (-1);
				}
				assert(r >= 0);
				assert((size_t)r <= *isz);
				ibuf += r;
				*isz -= (size_t)r;

				if (*isz == 0)
					fds[i].events = fds[i].revents = 0;
				if (*isz == 0 && complete) {
					assert(fds[i].fd == *out);
					closef(fds[i].fd);
					*out = fds[i].fd;
				}
				continue;
			}
			if (fds[i].revents & POLLIN) {
				r = read(fds[i].fd, obuf, obufsz);
				if (r < 0) {
					perror("read");
					return (-1);
				}
				assert(r >= 0);
				assert((size_t)r <= obufsz);
				obuf += r;
				osz += r;
				obufsz -= (size_t)r;
			}
			if (fds[i].revents & POLLHUP) {
				DBG("read hup at %zd\n", osz);
				return (osz);
			}
		}
	}
	DBG("read bottom at %zd\n", osz);
	return (osz);
}

static int
child_status(pid_t pid)
{
	int status;
	pid_t r;

	r = waitpid(pid, &status, WNOHANG);
	assert(r == 0 || r == pid);

	return (status);
}

static void *
splicer_fallback(const int *fds)
{
	const size_t bufsz = FALLBACK_BUF;
	char *buf = malloc(bufsz);
	ssize_t r;

	AN(buf);

	while (1) {
		r = read(fds[0], buf, bufsz);
		assert(r >= 0);
		if (r == 0)
			break;
		r = write(fds[1], buf, (size_t)r);
		if (r < 0)
			perror("fallback write error");
		assert(r >= 0);
		assert(r >= 0);
	}

	free(buf);
	return (NULL);
}

/*
 * assumes output is always blocking
 */
static void *
splicer(void *a)
{
	int *fds = a;
#if HAVE_SPLICE
	ssize_t r;

	do {
		r = splice(fds[0], NULL, fds[1], NULL, SIZE_MAX,
		    SPLICE_F_MORE | SPLICE_F_MOVE);
		if (r == -1) {
			if (errno == EINVAL)
				return (splicer_fallback(fds));
			else
				perror("splicer unexpected error");
		}
		assert(r >= 0);
	} while (r != 0);

	return (NULL);
#else
	return (splicer_fallback(fds));
#endif
}

static pthread_t
splicethread(int fds[2])
{
	pthread_t tid;

	AZ(pthread_create(&tid, NULL, splicer, fds));
	return (tid);
}

char *ibuf = NULL, *obuf = NULL;

static void
cleanup(void)
{
	AN(ibuf);
	AN(obuf);
	free(ibuf);
	free(obuf);
}

int
main(int argc, char *argv[], char * const envp[])
{
	size_t ibufsz = DEF_IBUF, obufsz = DEF_OBUF, min = 0,
	    isz = 0, tryisz, osz;
	char *dir = ".";
	ssize_t r = 0;
	char *ibufr;
	int icompl, i, opt, finc;
	char **tryv, **finv, **finv_fn;
	char *ep;
	int spl_in[2], spl_out[2];
	int writefd, readfd;
	pid_t pid;
	pthread_t thr;

	while ((opt = getopt(argc, argv, "i:o:m:d:")) != -1) {
		ep = NULL;
		switch (opt) {
		case 'i':
			ibufsz = strtoul(optarg, &ep, 10);
			if (*ep != '\0')
				argerr(argv, "-i", ep);
			break;
		case 'o':
			obufsz = strtoul(optarg, &ep, 10);
			if (*ep != '\0')
				argerr(argv, "-o", ep);
			break;
		case 'm':
			min = strtoul(optarg, &ep, 10);
			if (*ep != '\0')
				argerr(argv, "-m", ep);
			break;
		case 'd':
			dir = optarg;
			break;
		default: /* '?' */
			usage(argv);
			exit(EXIT_FAILURE);
		}
	}
	if (optind >= argc) {
		fprintf(stderr, "try command missing\n");
		usage(argv);
		exit(EXIT_FAILURE);
	}
	if (ibufsz == 0) {
		fprintf(stderr, "-i argument can not be zero\n");
		exit(EXIT_FAILURE);
	}
	if (obufsz == 0) {
		fprintf(stderr, "-o argument can not be zero\n");
		exit(EXIT_FAILURE);
	}
	if (*dir == '\0') {
		fprintf(stderr, "-d argument can not be empty\n");
		exit(EXIT_FAILURE);
	}

	tryv = &argv[optind];

	while (optind < argc && strcmp(argv[optind], "----"))
		optind++;

	if (optind >= argc) {
		fprintf(stderr, "fin command missing\n");
		usage(argv);
		exit(EXIT_FAILURE);
	}
	argv[optind] = NULL;
	optind++;

	finv = &argv[optind];
	finc = argc - optind;

	for (i = 0; i < finc; i++) {
		AN(finv[i]);
		if (! strcmp(finv[i], "%f"))
			break;
	}
	if (i == finc) {
		errno = EINVAL;
		pexit("no %f in final command");
	}
	finv_fn = &finv[i];

	ibuf = malloc(ibufsz);
	AN(ibuf);
	obuf = malloc(obufsz);
	AN(obuf);

	while (isz < ibufsz) {
		r = read(STDIN_FILENO, ibuf + isz, ibufsz - isz);
		if (r < 0) {
			cleanup();
			pexit("read(STDIN_FILENO)");
		}
		if (r == 0)
			break;
		assert(r > 0);
		isz += (size_t)r;
	}
	if (isz == 0) {
		cleanup();
		pexit("no input bytes");
	}
	icompl = (r == 0);

	pid = subproc(tryv, envp, &writefd, &readfd);

	/*
	 * - write ibuf, read obuf
	 * - if tryv exits before ibuf written, fall back
	 * - if tryv finishes when obuf is full, success
	 */
	tryisz = isz;
	r = trystream((const char *)ibuf, &tryisz, obuf, obufsz,
	    &writefd, &readfd, icompl);
	DBG("min %zu osz %zd readfd %d writefd %d\n",
	    min, r, readfd, writefd);
	if (r < 0)
		osz = 0;
	else
		osz = (size_t)r;
	if (r < 0 || osz < min) {
		(void) kill(pid, SIGKILL);
		(void) waitpid(pid, &i, 0);
	} else {
		i = child_status(pid);
	}
	if (osz < min || (WIFEXITED(i) && WEXITSTATUS(i) > 0)) {
		char fn[FILENAME_MAX];

		fprintf(stderr, "ELBEstream: using file fallback\n");

		closefif(readfd);
		closefif(writefd);

		bprintf(fn, "%s/elbebuf.XXXXXX", dir);
		writefd = mkstemp(fn);
		if (writefd < 0) {
			fprintf(stderr, "mkstemp(%s) failed: %s (%d)\n\n",
			    fn, strerror(errno), errno);
			cleanup();
			exit(EXIT_FAILURE);
		}
		assert(writefd >= 0);
		if (isz > 0) {
			r = write(writefd, ibuf, isz);
			if (r < 0) {
				perror("write(tmpfile)");
				(void) unlink(fn);
				cleanup();
				exit(EXIT_FAILURE);
			}
			assert ((size_t)r == isz);
		}
		spl_in[0] = STDIN_FILENO;
		spl_in[1] = writefd;
		(void) splicer(spl_in);
		closef(writefd);

		AN(finv_fn);
		*finv_fn = fn;

		// writes to STDOUT_FILENO happen directly in the exec'ed
		// process
		pid = fork();
		if (pid < 0) {
			cleanup();
			pexit("fork");
		}
		if (pid == 0) {
			if (execve(finv[0], finv, envp) == -1) {
				pexit("execve(finv)");
				cleanup();
			}
		}
		AZ(close(STDIN_FILENO));
		AZ(close(STDOUT_FILENO));
		(void) waitpid(pid, &i, 0);
		// if it was not for this unlink, we could just execve() :(
		(void) unlink(fn);
		cleanup();
		exit (WEXITSTATUS(i));
	}
	/* tryisz: bytes left unconsumed after trystream() */
	assert(tryisz <= isz);
	ibufr = ibuf + (isz - tryisz);
	isz = tryisz;

	if (osz > 0 && vmwrite(STDOUT_FILENO, obuf, osz) < 0) {
		cleanup();
		pexit("write stdout");
	}

	thr = pthread_self();
	if (readfd >= 0) {
		spl_out[0] = readfd;
		spl_out[1] = STDOUT_FILENO;
		thr = splicethread(spl_out);
		AN(thr);
	}

	if (writefd >= 0) {
		blockf(writefd);
		if (isz > 0 && vmwrite(writefd, ibufr, isz) < 0) {
			perror("write bufffer to trycmd");
		} else {
			spl_in[0] = STDIN_FILENO;
			spl_in[1] = writefd;
			(void) splicer(spl_in);
		}
		closef(writefd);
	}

	(void) waitpid(pid, &i, 0);

	if (readfd >= 0) {
		assert(thr != pthread_self());
		AZ(pthread_join(thr, NULL));
	}

	cleanup();
	exit (WEXITSTATUS(i));
}
